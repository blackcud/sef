const int IBE_UI_DG_COLUMN_MAX = 16;
const int IBE_UI_DG_KEY_MAX = 1024;
const int IBE_UI_DG_VIEW_ROW_MAX = 20;

const int IBE_UI_DG_COL_LAST_CREATED = -1;

int ibe_ui_dg_prototype_row_count();
text ibe_ui_dg_prototype_get_text(int idx, int col, int row);
text ibe_ui_dg_prototype_get_tooltip(int idx, int col, int row);
int ibe_ui_dg_prototype_get_sort_value(int idx, int col);
bool ibe_ui_dg_prototype_filter_item(int idx);

typedef funcref<ibe_ui_dg_prototype_row_count> ibe_ui_dg_prototype_row_count_t;
typedef funcref<ibe_ui_dg_prototype_get_text> ibe_ui_dg_prototype_get_text_t;
typedef funcref<ibe_ui_dg_prototype_get_tooltip> ibe_ui_dg_prototype_get_tooltip_t;
typedef funcref<ibe_ui_dg_prototype_get_sort_value> ibe_ui_dg_prototype_get_sort_value_t;
typedef funcref<ibe_ui_dg_prototype_filter_item> ibe_ui_dg_prototype_filter_item_t;

struct ibe_ui_dg_state_t {
    int sort_key;
    bool sort_asc;
    int[IBE_UI_DG_KEY_MAX] keys;
    int key_count;
    int view_offset;
};

struct ibe_ui_dg_column_t {
    bool exists;
    text label;
    bool sortable;
    fixed size_hint;
    int min_width;
    int max_width;
    int final_width;
};

struct ibe_ui_dg_view_th_t {
    int control;
    int arrow_up;
    int arrow_down;
};

struct ibe_ui_dg_view_td_t {
    int control;
};

struct ibe_ui_dg_view_t {
    int panel;
    int table_width;
    int table_height;
    int row_visible_count;
    int pagination_panel;
    int pagination_info;
    int pagination_prev;
    int pagination_next;
    ibe_ui_dg_view_th_t[IBE_UI_DG_COLUMN_MAX] headers;
    ibe_ui_dg_view_td_t[IBE_UI_DG_VIEW_ROW_MAX][IBE_UI_DG_COLUMN_MAX] cells;
};

struct ibe_ui_dg_t {
    ibe_ui_dg_prototype_row_count_t fn_row_count;
    ibe_ui_dg_prototype_get_text_t fn_get_text;
    ibe_ui_dg_prototype_get_tooltip_t fn_get_tooltip;
    ibe_ui_dg_prototype_get_sort_value_t fn_get_sort_value;
    ibe_ui_dg_prototype_filter_item_t fn_filter_item;
    bool fne_filter_item;
    trigger trig_ui_event_proxy;
    ibe_ui_dg_column_t[IBE_UI_DG_COLUMN_MAX] columns;
    int column_count;
    ibe_ui_dg_state_t[MAX_PLAYERS + 1] states;
    ibe_ui_dg_view_t view;
};
