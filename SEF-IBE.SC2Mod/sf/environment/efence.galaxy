static const int SF_ENVI_EFENCE_LINK_LIMIT = 64;

const int SF_ENVI_EFENCE_LTYPE_ANY = 0;
const int SF_ENVI_EFENCE_LTYPE_ELECTRICITY = 1;
const int SF_ENVI_EFENCE_LTYPE_SWITCHER = 2;
const int SF_ENVI_EFENCE_LTYPE_EXTERNAL_SWITCHER = 3;

struct sf_envi_efence_link_t {
    int link_type;
    bool active;
    unitgroup pillars;
    actor[3] beams;
    unitgroup active_heroes;
};

struct sf_envi_efence_t {
    int[SF_ENVI_EFENCE_LINK_LIMIT] link_refs;
};

static sf_envi_efence_t[SF_ENVI_OBJECT_INSTANCE_LIMIT] sf_envi_efences;
static sf_envi_efence_link_t[SF_ENVI_EFENCE_LINK_LIMIT] sf_envi_efence_links;
static trigger sf_envi_efence_trig_periodic = null;

static unitfilter eltricity_matching_filter = UnitFilter (
    (1 << (c_targetFilterGround)) | (1 << (c_targetFilterBiological)),
    0,
    (1 << (c_targetFilterArmored)),
    (1 << (c_targetFilterDead - 32)) | (1 << (c_targetFilterHidden - 32)) | (1 << (c_targetFilterInvulnerable - 32))
);

static void sf_envi_efence_link_destroy(int link_id);

static void sf_envi_efence_on_destroy(int obj_id)
{
    int i;

    if (sf_envi_efence_trig_periodic != null) {
        TriggerStop(sf_envi_efence_trig_periodic);
        TriggerDestroy(sf_envi_efence_trig_periodic);
        sf_envi_efence_trig_periodic = null;
        Wait(TICK_PERIOD, c_timeGame);

        for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
            if (sf_envi_efence_links[i].link_type == 0) { continue; }
            sf_envi_efence_link_destroy(i);
        }
    }

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        sf_envi_efences[obj_id].link_refs[i] = -1;
    }
}

static actor sf_envi_efence_beam_create(string model, unit unit_a, string ref_point_a, unit unit_b, string ref_point_b)
{
    actor beam;

    beam = LibWH_FUNC_BS_BeamCreate(
        "BeamSimple",
        model,
        LibWH_FUNC_BS_CreateBeamPointAtActor(libNtve_gf_MainActorofUnit(unit_a), ref_point_a),
        LibWH_FUNC_BS_CreateBeamPointAtActor(libNtve_gf_MainActorofUnit(unit_b), ref_point_b)
    );
    ActorSend(beam, "AnimBracketStart Spell Birth Stand Death");
    ActorSend(beam, "AnimPlay GLStand GLstand PlayForever");
    // ActorSend(beam, "SetTintColor {255,0,0 2.000000}");

    return beam;
}

static void sf_envi_efence_link_destroy(int link_id)
{
    int i;

    Dbg("[efence/link_destroy] link_id = " + IntToString(link_id));

    sf_envi_efence_links[link_id].link_type = 0;
    sf_envi_efence_links[link_id].active = false;
    sf_envi_efence_links[link_id].active_heroes = null;
    sf_envi_efence_links[link_id].pillars = null;
    for (i = 0; i < 3; i += 1) {
        if (sf_envi_efence_links[link_id].beams[i] != null) {
            ActorSend(sf_envi_efence_links[link_id].beams[i], "Destroy");
            sf_envi_efence_links[link_id].beams[i] = null;
        }
    }
}

static void sf_envi_efence_link_activate(int link_id, bool activate)
{
    int i;

    sf_envi_efence_links[link_id].active = activate;

    for (i = 0; i < 3; i += 1) {
        if (sf_envi_efence_links[link_id].beams[i] == null) { continue; }

        if (activate) {
            ActorSend(sf_envi_efence_links[link_id].beams[i], "SetOpacity 1.000000 0.050000");
            // SoundPlayOnUnitForPlayer(SoundLink("PsiPowerLink_Discharge_Amb", -1), c_maxPlayers, PlayerGroupAll(), lp_unit, 0.0, 100.0, 0.0);
            // enter
            // SoundPlayForPlayer(SoundLink("PsiPowerLink_Discharge_Amb", -1), c_maxPlayers, PlayerGroupAll(), 100.0, 0.0);

            // out
            // SoundPlayForPlayer(SoundLink("PsiPowerLink_ServoTurret", -1), c_maxPlayers, PlayerGroupAll(), 100.0, 0.0);
        }
        else {
            ActorSend(sf_envi_efence_links[link_id].beams[i], "SetOpacity 0.000000 0.150000");
        }
    }
}

static bool is_crossing_link(structref<sf_envi_efence_link_t> link, unit cr_unit)
{
    unit pil_a;
    unit pil_b;
    fixed pil_distance;
    fixed self_distance;
    fixed angle;

    if (!UnitIsAlive(cr_unit)) { return false; }
    if (sf_phys_is_unit_during_jump(cr_unit)) { return false; }

    pil_a = UnitGroupUnit(link.pillars, 1);
    pil_b = UnitGroupUnit(link.pillars, 2);
    pil_distance = DistanceBetweenPoints(UnitGetPosition(pil_a), UnitGetPosition(pil_b));
    self_distance = DistanceBetweenPoints(UnitGetPosition(cr_unit), UnitGetPosition(pil_a));
    angle = AngleBetweenPoints(
        UnitGetPosition(pil_a),
        UnitGetPosition(cr_unit)) - AngleBetweenPoints(UnitGetPosition(pil_a), UnitGetPosition(pil_b)
    );

    if (
        (AbsF((Cos(angle) * self_distance)) <= pil_distance) &&
        (AbsF((Sin(angle) * self_distance)) <= 0.3) &&
        (Cos(angle) >= 0.0)
    ) {
        return true;
    }

    return false;
}

static void sf_envi_efence_check_interact(int link_id)
{
    structref<HERO> hero;
    structref<sf_envi_efence_link_t> link;
    unit pil_a;
    unit pil_b;
    fixed pil_distance;
    int i;
    unitgroup ugroup;
    unit tmp_unit;

    link = sf_envi_efence_links[link_id];
    pil_a = UnitGroupUnit(link.pillars, 1);
    pil_b = UnitGroupUnit(link.pillars, 2);
    pil_distance = DistanceBetweenPoints(UnitGetPosition(pil_a), UnitGetPosition(pil_b));

    for (i = 1; i <= UnitGroupCount(gm_herogroup, c_unitCountAll); i += 1) {
        hero = gm_players[UnitGetOwner(UnitGroupUnit(gm_herogroup, i))].hero;

        if (is_crossing_link(link, hero.mainUnit)) {
            if (link.link_type == SF_ENVI_EFENCE_LTYPE_ELECTRICITY) {
                UnitKill(hero.mainUnit);
            }
            else {
                if (!UnitGroupHasUnit(link.active_heroes, hero.mainUnit)) {
                    UnitGroupAdd(link.active_heroes, hero.mainUnit);
                    SoundPlayOnUnitForPlayer(SoundLink("PsiPowerLink_Discharge_Amb", -1), c_maxPlayers, PlayerGroupAll(), hero.mainUnit, 0.0, 100.0, 0.0);
                    ActorSend(sf_envi_efence_links[link_id].beams[0], "SetOpacity 0.200000 0.200000");

                    if (link.link_type == SF_ENVI_EFENCE_LTYPE_SWITCHER) {
                        sf_envi_efence_pillar_toggle(sf_envi_object_get_by_unit(pil_a));
                        sf_envi_efence_pillar_toggle(sf_envi_object_get_by_unit(pil_b));
                    }
                    else if (link.link_type == SF_ENVI_EFENCE_LTYPE_EXTERNAL_SWITCHER) {
                        sf_core_event_prepare("efence_link_crossed");
                        sf_core_event_set_int("player", hero.player);
                        sf_core_event_set_int("link", link_id);
                        sf_core_event_set_int("pillar_a", sf_envi_object_get_by_unit(pil_a));
                        sf_core_event_set_int("pillar_b", sf_envi_object_get_by_unit(pil_b));
                        sf_core_event_send();
                    }
                }
            }
        }
        else if (UnitGroupHasUnit(link.active_heroes, hero.mainUnit)) {
            UnitGroupRemove(link.active_heroes, hero.mainUnit);
            ActorSend(sf_envi_efence_links[link_id].beams[0], "SetOpacity 1.000000 0.200000");
        }
    }

    if (link.link_type == SF_ENVI_EFENCE_LTYPE_ELECTRICITY) {
        ugroup = UnitGroupSearch(
            null,
            c_playerAny,
            UnitGetPosition(pil_a),
            pil_distance,
            eltricity_matching_filter,
            c_noMaxCount
        );
        for (i = 1; i <= UnitGroupCount(ugroup, c_unitCountAll); i += 1) {
            tmp_unit = UnitGroupUnit(ugroup, i);

            if (is_crossing_link(link, tmp_unit)) {
                UnitKill(tmp_unit);
            }
        }
    }
}

static void sf_envi_efence_store_link_ref(int obj_id, int link_id)
{
    int i;

    Dbg("[efence/store_link_ref] obj_id = " + IntToString(obj_id) + " ; link_id = " + IntToString(link_id));

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        if (sf_envi_efences[obj_id].link_refs[i] != -1) { continue; }
        sf_envi_efences[obj_id].link_refs[i] = link_id;
        return;
    }

    Dbg("[efence/store_link_ref] failed to insert - refs array is full");
}

static void sf_envi_efence_remove_link_ref(int obj_id, int link_id)
{
    int i;

    Dbg("[efence/remove_link_ref] obj_id = " + IntToString(obj_id) + " ; link_id = " + IntToString(link_id));

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        if (sf_envi_efences[obj_id].link_refs[i] != link_id) { continue; }
        sf_envi_efences[obj_id].link_refs[i] = -1;
        return;
    }

    Dbg("[efence/remove_link_ref] failed to remove - link not found in refs");
}

void sf_envi_efence_init()
{
    sf_envi_object_type_t obj_type;

    obj_type.name = "efence";
    obj_type.fn_destroy = sf_envi_efence_on_destroy;
    sf_envi_object_register_type_at(obj_type, SF_ENVI_OBJECT_TYPE_EFENCE);
}

int sf_envi_efence_create(point pos)
{
    int obj_id;
    int i;

    obj_id = sf_envi_object_create_main_at(SF_ENVI_OBJECT_TYPE_EFENCE, "ElectricFencePillar", pos);
    sf_make_unit_selectable(sf_envi_object_instances[obj_id].main_unit, false);

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        sf_envi_efences[obj_id].link_refs[i] = -1;
    }

    if (sf_envi_efence_trig_periodic == null) {
        sf_envi_efence_trig_periodic = TriggerCreate("sf_envi_efence_on_periodic");
        TriggerExecute(sf_envi_efence_trig_periodic, false, false);
    }

    return obj_id;
}

void sf_envi_efence_link(int obj_id, int foreign_obj_id, int link_type, bool active)
{
    int i;
    int link_id = -1;
    unit pil_a = sf_envi_object_instances[obj_id].main_unit;
    unit pil_b = sf_envi_object_instances[foreign_obj_id].main_unit;

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        if (sf_envi_efence_links[i].pillars != null) { continue; }
        link_id = i;
        break;
    }

    if (link_id == -1) {
        Dbg("[efence/link] overflow");
        return;
    }

    sf_envi_efence_links[link_id].pillars = UnitGroupEmpty();
    sf_envi_efence_links[link_id].active_heroes = UnitGroupEmpty();
    UnitGroupAdd(sf_envi_efence_links[link_id].pillars, pil_a);
    UnitGroupAdd(sf_envi_efence_links[link_id].pillars, pil_b);

    sf_envi_efence_links[link_id].link_type = link_type;
    sf_envi_efence_links[link_id].active = active;

    if (link_type == SF_ENVI_EFENCE_LTYPE_ELECTRICITY) {
        sf_envi_efence_links[link_id].beams[0] = sf_envi_efence_beam_create("ElectricFenceBeam", pil_a, "Ref_Hardpoint", pil_b, "Ref_Hardpoint");
        sf_envi_efence_links[link_id].beams[1] = sf_envi_efence_beam_create("ElectricFenceBeam", pil_a, "Ref_Hardpoint 01", pil_b, "Ref_Hardpoint 01");
        sf_envi_efence_links[link_id].beams[2] = sf_envi_efence_beam_create("ElectricFenceBeam", pil_a, "Ref_Hardpoint 02", pil_b, "Ref_Hardpoint 02");
    }
    else {
        sf_envi_efence_links[link_id].beams[0] = sf_envi_efence_beam_create("PointDefenseDroneAttackBeam", pil_a, "Ref_Hardpoint", pil_b, "Ref_Hardpoint");
        // sf_envi_efence_links[link_id].beams[0] = sf_envi_efence_beam_create("NanoRepairBeam", pil_a, "Ref_Hardpoint", pil_b, "Ref_Hardpoint");
        // sf_envi_efence_links[link_id].beams[1] = sf_envi_efence_beam_create("NanoRepairBeam", pil_b, "Ref_Hardpoint", pil_a, "Ref_Hardpoint");
    }

    if (!active) {
        sf_envi_efence_link_activate(link_id, active);
    }

    sf_envi_efence_store_link_ref(obj_id, link_id);
    sf_envi_efence_store_link_ref(foreign_obj_id, link_id);
}

void sf_envi_efence_unlink(int obj_id, int foreign_obj_id, int link_type)
{
    int i;
    int link_id;
    int obj_paired_id = -1;
    unitgroup ug_tmp;
    structref<sf_envi_efence_link_t> link;

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        link_id = sf_envi_efences[obj_id].link_refs[i];
        if (link_id == -1) { continue; }

        link = sf_envi_efence_links[link_id];
        if (SF_ENVI_EFENCE_LTYPE_ANY != link_type && link.link_type != link_type) { continue; }
        if (foreign_obj_id != -1 && !UnitGroupHasUnit(link.pillars, sf_envi_object_instances[foreign_obj_id].main_unit)) { continue; }

        ug_tmp = UnitGroupCopy(link.pillars);
        UnitGroupRemove(ug_tmp, sf_envi_object_instances[obj_id].main_unit);
        obj_paired_id = sf_envi_object_get_by_unit(UnitGroupUnit(ug_tmp, 1));

        sf_envi_efence_remove_link_ref(obj_id, link_id);
        sf_envi_efence_remove_link_ref(obj_paired_id, link_id);

        sf_envi_efence_link_destroy(link_id);
    }
}

void sf_envi_efence_unlink_all(int obj_id, int link_type)
{
    sf_envi_efence_unlink(obj_id, -1, link_type);
}

bool sf_envi_efence_pillar_electricity_status(int obj_id, int foreign_obj_id)
{
    int i;
    int link_id;
    unit foregin_pillar = sf_envi_object_get_base_unit(foreign_obj_id);

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        link_id = sf_envi_efences[obj_id].link_refs[i];
        if (link_id == -1) { continue; }
        if (!UnitGroupHasUnit(sf_envi_efence_links[link_id].pillars, foregin_pillar)) { continue; }
        if (sf_envi_efence_links[link_id].link_type != SF_ENVI_EFENCE_LTYPE_ELECTRICITY) { continue; }
        return sf_envi_efence_links[link_id].active;
    }

    Dbg("[efence/pillar_electricity_status] link not found between objects: A = " + IntToString(obj_id) + " B = " + IntToString(foreign_obj_id));

    return false;
}

void sf_envi_efence_pillar_electricity_set(int obj_id, int foreign_obj_id, bool active)
{
    int i;
    int link_id;
    unit foregin_pillar = sf_envi_object_get_base_unit(foreign_obj_id);

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        link_id = sf_envi_efences[obj_id].link_refs[i];
        if (link_id == -1) { continue; }
        if (!UnitGroupHasUnit(sf_envi_efence_links[link_id].pillars, foregin_pillar)) { continue; }
        if (sf_envi_efence_links[link_id].link_type != SF_ENVI_EFENCE_LTYPE_ELECTRICITY) { continue; }
        sf_envi_efence_link_activate(link_id, active);
        return;
    }

    Dbg("[efence/pillar_electricity_set] link not found between objects: A = " + IntToString(obj_id) + " B = " + IntToString(foreign_obj_id));
}

void sf_envi_efence_pillar_toggle(int obj_id)
{
    int i;
    int link_id;

    for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
        link_id = sf_envi_efences[obj_id].link_refs[i];
        if (link_id == -1) { continue; }
        if (sf_envi_efence_links[link_id].link_type != SF_ENVI_EFENCE_LTYPE_ELECTRICITY) { continue; }
        sf_envi_efence_link_activate(link_id, !sf_envi_efence_links[link_id].active);
    }
}

void sf_envi_add_event_efence_link_crossed(trigger trig)
{
    sf_core_event_attach(trig, "efence_link_crossed");
}

int sf_envi_event_param_efence_player()
{
    return sf_core_event_get_int("player");
}

int sf_envi_event_param_efence_link()
{
    return sf_core_event_get_int("link");
}

int sf_envi_event_param_efence_pillar_a()
{
    return sf_core_event_get_int("pillar_a");
}

int sf_envi_event_param_efence_pillar_b()
{
    return sf_core_event_get_int("pillar_b");
}

// ===
// ===

bool sf_envi_efence_on_periodic(bool test_conds, bool run_actions)
{
    int i;

    for (;;) {
        for (i = 0; i < SF_ENVI_EFENCE_LINK_LIMIT; i += 1) {
            if (!sf_envi_efence_links[i].active) { continue; }

            sf_envi_efence_check_interact(i);
        }

        Wait(TICK_PERIOD, c_timeGame);
    }

    return true;
}

