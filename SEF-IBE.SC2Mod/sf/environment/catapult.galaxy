struct sf_envi_catapult_t {
    unit cata_unit;
    region rg_act;
    point destination;
    trigger trig;
    bool auto_launch;
    fixed height_boost;
    fixed lateral_offset;
};

sf_envi_catapult_t [SF_ENVI_OBJECT_INSTANCE_LIMIT]sf_envi_catapults;

static void sf_envi_catapult_on_destroy(int obj_id)
{
    if (TriggerGetFunction(sf_envi_catapults[obj_id].trig) != null) {
        TriggerDestroy(sf_envi_catapults[obj_id].trig);
    }
    sf_envi_catapults[obj_id].rg_act = null;
    sf_envi_catapults[obj_id].destination = null;
    sf_envi_catapults[obj_id].cata_unit = null;
    sf_envi_catapults[obj_id].trig = null;
    sf_envi_catapults[obj_id].height_boost = 0.0;
    sf_envi_catapults[obj_id].lateral_offset = 0.0;
}

static void sf_envi_catapult_process_launch(int obj_id, unitgroup ugroup, bool force_launch)
{
    structref<sf_envi_catapult_t> cata;
    unitgroup tmp_group;
    structref<HERO> hero;
    fixed approach_angle;
    point destination;
    unit tmp_unit;
    int i;
    int launch_counter = 0;
    sf_phys_jump_info_t jump_info;
    string animation;

    cata = sf_envi_catapults[obj_id];

    if (ugroup == null) {
        ugroup = UnitGroup(
            null,
            c_playerAny,
            cata.rg_act,
            UnitFilter(0, 0, (1 << c_targetFilterMissile), (1 << (c_targetFilterDead - 32)) | (1 << (c_targetFilterHidden - 32))),
            c_noMaxCount
        );
    }

    for (i = 1; i <= UnitGroupCount(ugroup, c_unitCountAll); i += 1) {
        tmp_unit = UnitGroupUnit(ugroup, i);

        if (tmp_unit == cata.cata_unit) { continue; }
        if (!UnitTestState(tmp_unit, c_unitStateRadarable)) { continue; }
        if (sf_phys_is_unit_during_jump(tmp_unit)) {
            sf_phys_get_unit_jump_info(tmp_unit, jump_info);
            if (!RegionContainsPoint(cata.rg_act, jump_info.end_point)) {
                continue;
            }

            UnitSetCustomValue(jump_info.munit, SF_GAME_UVAL_PREPARE_CHAIN_JUMP, 1.0);
            while (sf_phys_is_unit_during_jump(tmp_unit)) {
                Wait(TICK_PERIOD, c_timeGame);
            }
            // aborted - i.e. shade
            if (UnitGetCustomValue(jump_info.munit, SF_GAME_UVAL_PREPARE_CHAIN_JUMP) == 0.0) {
                continue;
            }
        }

        UnitIssueOrder(tmp_unit, Order(AbilityCommand("stop", 0)), c_orderQueueReplace);

        if (UnitGroupHasUnit(gm_herogroup, tmp_unit)) {
            hero = gm_players[gm_getPlayerByUnit(tmp_unit)].hero;
            // if (gm_players[UnitGetOwner(hero.mainUnit)].options.cameraPan) {
            //     CameraPan(UnitGetOwner(hero.mainUnit), cata.destination, 0.3, 0.6, 1.0, true);
            // }
            approach_angle = UnitGetFacing(hero.guideUnit);
        }
        else {
            approach_angle = UnitGetFacing(tmp_unit);
        }

        if (sf_phys_is_point_on_ice(UnitGetPosition(tmp_unit))) {
            destination = PointWithOffsetPolar(
                cata.destination,
                cata.lateral_offset,
                AngleBetweenPoints(UnitGetPosition(tmp_unit), cata.destination) - (AngleBetweenPoints(UnitGetPosition(tmp_unit), cata.destination) - approach_angle)
            );
        }
        else {
            destination = cata.destination;
        }

        sf_phys_jump_launch(tmp_unit, destination, 0.0, cata.height_boost);
        launch_counter += 1;
    }

    if (launch_counter > 0 || force_launch) {
        // if ((lp_animationName == ge_catapultAnimationType__90degRotation)) {
        //     lv_animName = "Far";
        // }
        // else if ((lp_animationName == ge_catapultAnimationType__60degRotation)) {
        //     lv_animName = "Fast";
        // }
        // else if ((lp_animationName == ge_catapultAnimationType__45degRotation)) {
        //     lv_animName = "Fidget";
        // }
        // else if ((lp_animationName == ge_catapultAnimationType__40degRotation)) {
        //     lv_animName = "Fire";
        // }
        // else if ((lp_animationName == ge_catapultAnimationType__30degRotation)) {
        //     lv_animName = "Flail";
        // }
        // else if ((lp_animationName == ge_catapultAnimationType__20degRotation)) {
        //     lv_animName = "Fling";
        // }
        // else if ((lp_animationName == ge_catapultAnimationType__10degRotation)) {
        //     lv_animName = "Fly";
        // }
        // else if ((lp_animationName == ge_catapultAnimationType__0degRotationUpPush)) {
        //     lv_animName = "Forward";
        // }
        animation = "Far";
        if (cata.height_boost >= -2.5) {
            animation = "Far";
        }
        if (cata.height_boost >= -1.5) {
            animation = "Fast";
        }
        if (cata.height_boost >= -0.5) {
            animation = "Fidget";
        }
        if (cata.height_boost >= 0.5) {
            animation = "Fire";
        }
        if (cata.height_boost >= 1.5) {
            animation = "Flail";
        }
        if (cata.height_boost >= 2.5) {
            animation = "Fling";
        }
        if (cata.height_boost >= 3.5) {
            animation = "Fly";
        }
        if (cata.height_boost >= 4.5) {
            animation = "Forward";
        }

        libNtve_gf_PlayAnimation(libNtve_gf_MainActorofUnit(cata.cata_unit), c_animNameDefault, animation, 0, c_animTimeDefault);
        libNtve_gf_SetAnimationTimeScale(libNtve_gf_MainActorofUnit(cata.cata_unit), c_animNameDefault, 4.0);
        SoundPlayAtPoint(SoundLink("CatapultClunk", -1), PlayerGroupAll(), UnitGetPosition(cata.cata_unit), 0.1, 60.0, 0.0);
    }
}

void sf_envi_catapult_init()
{
    sf_envi_object_type_t obj_type;

    obj_type.name = "catapult";
    obj_type.fn_destroy = sf_envi_catapult_on_destroy;
    sf_envi_object_register_type_at(obj_type, SF_ENVI_OBJECT_TYPE_CATAPULT);
}

int sf_envi_catapult_create_from_unit(unit cata_unit, region rg_act, point destination)
{
    int obj_id;
    structref<sf_envi_catapult_t> cata;

    obj_id = sf_envi_object_create(SF_ENVI_OBJECT_TYPE_CATAPULT);
    cata = sf_envi_catapults[obj_id];

    sf_envi_object_instances[obj_id].main_unit = cata_unit;
    UnitGroupAdd(sf_envi_object_instances[obj_id].ugroup, sf_envi_object_instances[obj_id].main_unit);

    cata.cata_unit = cata_unit;
    cata.rg_act = rg_act;
    cata.destination = destination;
    cata.auto_launch = true;
    cata.lateral_offset = 1.3;
    if (TriggerFind("sf_envi_on_catapult_enter") == null) {
        TriggerCreate("sf_envi_on_catapult_enter");
    }
    cata.trig = TriggerFind("sf_envi_on_catapult_enter");
    sf_envi_object_instances[obj_id].preserve_main_unit = true;
    TriggerAddEventUnitRegion(cata.trig, null, cata.rg_act, true);

    return obj_id;
}

void sf_envi_catapult_set_auto_launch(int obj_id, bool auto_launch)
{
    sf_envi_catapults[obj_id].auto_launch = auto_launch;
}

void sf_envi_catapult_set_height_boost(int obj_id, fixed height_boost)
{
    sf_envi_catapults[obj_id].height_boost = height_boost;
}

void sf_envi_catapult_set_lateral_offset(int obj_id, fixed lateral_offset)
{
    sf_envi_catapults[obj_id].lateral_offset = lateral_offset;
}


void sf_envi_catapult_launch(int obj_id)
{
    sf_envi_catapult_process_launch(obj_id, null, true);
}

bool sf_envi_on_catapult_enter(bool testConds, bool runActions) {
    int i;
    int obj_id;
    region tmp_rg;
    unitgroup tmp_ugroup;

    tmp_rg = EventUnitRegion();

    // find out catapult obj id
    for (i = 0; i < SF_ENVI_OBJECT_INSTANCE_LIMIT; i += 1) {
        if (sf_envi_catapults[i].rg_act == tmp_rg) {
            obj_id = i;
            break;
        }
    }
    if (i == SF_ENVI_OBJECT_INSTANCE_LIMIT) {
        return false;
    }

    if (!sf_envi_catapults[i].auto_launch) {
        return false;
    }

    if (UnitGetType(EventUnit()) == "ShapeTorus") {
        return false;
    }

    UnitGroupAdd(tmp_ugroup, EventUnit());

    sf_envi_catapult_process_launch(obj_id, tmp_ugroup, false);

    return true;
}
